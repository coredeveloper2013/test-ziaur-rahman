<?php

Route::group(['middleware' => ['web','install', 'auth', 'package']], function(){
    Route::get('activities/clear', 'ActivityController@clear_activities');
    Route::resource('activities', 'ActivityController');
});