<?php

namespace App\Http\Controllers\Media;

use Illuminate\Http\Request;

use Auth;
use App\Models\Media\Media;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class MediaController extends Controller
{
    public function index()
    {
        //loaded media index page
      

        $medias = Media::get();

         $postMaxFileSize = str_replace('M', '',ini_get('post_max_size'));
        $uploadMaxFileSize = str_replace('M', '',ini_get('upload_max_filesize'));
        if($postMaxFileSize < $uploadMaxFileSize){
            $iniFileSize = $postMaxFileSize;
        }else{
            $iniFileSize = $uploadMaxFileSize;
        }
        return response()->json(get_defined_vars());
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Media $model)
    {
        $postMaxFileSize = str_replace('M', '',ini_get('post_max_size'));
        $uploadMaxFileSize = str_replace('M', '',ini_get('upload_max_filesize'));
        if($postMaxFileSize < $uploadMaxFileSize){
            $iniFileSize =  (float)$postMaxFileSize;
        }else{
            $iniFileSize = (float)$uploadMaxFileSize;
        }
        
        $this->validate($request, [
            'title' => 'required|unique:media',
            'media' => 'required|image|max:'.$iniFileSize*1024
        ]);

        $media = '';
        if ($request->file('media')) {
            $ext = $request->file('media')->getClientOriginalExtension();
            $media = microtime(true) . '.' . $ext;
            $destinationPath = 'upload';
            $request->file('media')->move($destinationPath, $media);
        }

        $all = $request->all();
        $all['user_id'] = Auth::id();
        $all['path'] = $media;
        $model = $model->create($all);

        return response()->json(['success' => true, 'media_info' => $model]);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Media $medium)
    {
        $medias = Media::where('user_id', Auth::id())->get();
        $title = trans('common.edit') . ' ' . trans('common.media');
        $postMaxFileSize = str_replace('M', '',ini_get('post_max_size'));
        $uploadMaxFileSize = str_replace('M', '',ini_get('upload_max_filesize'));
        if($postMaxFileSize < $uploadMaxFileSize){
            $iniFileSize = $postMaxFileSize;
        }else{
            $iniFileSize = $uploadMaxFileSize;
        }

        return response()->json(get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Media $medium, Request $request)
    {
        $postMaxFileSize = str_replace('M', '',ini_get('post_max_size'));
        $uploadMaxFileSize = str_replace('M', '',ini_get('upload_max_filesize'));
        if($postMaxFileSize < $uploadMaxFileSize){
            $iniFileSize =  (float)$postMaxFileSize;
        }else{
            $iniFileSize = (float)$uploadMaxFileSize;
        }
        
         $test = $this->validate($request, [
            'title' => 'required|unique:media,title,'. $medium->id,
            'media' => 'required|image|max:'.$iniFileSize*1024
        ]);

        $all = $request->all();
        $all['user_id'] = Auth::id();

        if ($request->hasFile('media')) {
            $ext = $request->file('media')->getClientOriginalExtension();
            $media = microtime(true) . '.' . $ext;
            $destinationPath = 'upload';
            $request->file('media')->move($destinationPath, $media);
            $all['path'] = $media;
        }


        $medium->update($all);
        return response()->json(['success' => 'successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Media $medium)
    {
        $medium->delete();
        return response()->json(['success' => 'successfully deleted']);
    }
}
